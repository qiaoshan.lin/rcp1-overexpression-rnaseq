#!/bin/bash
#SBATCH --job-name=salmon
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-11
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=30G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o salmon_%A_%a.out
#SBATCH -e salmon_%A_%a.err

export PATH=$PATH:~/local/apps/salmon-latest_linux_x86_64/bin/

file=(trimmed_LF10_FB13A trimmed_LF10_FB13B trimmed_LF10_FB13C trimmed_LF10_FB5A trimmed_LF10_FB5B trimmed_LF10_FB5C trimmed_RCP1_FB3A trimmed_RCP1_FB3B trimmed_RCP1_FB3C trimmed_RCP1_FB9A trimmed_RCP1_FB9B trimmed_RCP1_FB9C)

transcripts=/home/CAM/qlin/resource/LF10/LF10g_v2.0.codingseq.fa

salmon quant \
 --threads 8 \
 --gcBias \
 --seqBias \
 -t $transcripts -l A \
 -a ../t3/${file[$SLURM_ARRAY_TASK_ID]}Aligned.toTranscriptome.out.bam \
 -o ${file[$SLURM_ARRAY_TASK_ID]}_quant


