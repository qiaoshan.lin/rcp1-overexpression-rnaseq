#!/bin/bash
#SBATCH --job-name=STAR
#SBATCH -c 12
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-11
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=30G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o align_%A_%a.out
#SBATCH -e align_%A_%a.err

module load STAR
module load samtools

file=(trimmed_LF10_FB13A trimmed_LF10_FB13B trimmed_LF10_FB13C trimmed_LF10_FB5A trimmed_LF10_FB5B trimmed_LF10_FB5C trimmed_RCP1_FB3A trimmed_RCP1_FB3B trimmed_RCP1_FB3C trimmed_RCP1_FB9A trimmed_RCP1_FB9B trimmed_RCP1_FB9C)

STAR --runThreadN 12 \
 --genomeDir genome \
 --readFilesIn ../readsTrimming/${file[$SLURM_ARRAY_TASK_ID]}_1_paired.fq.gz ../readsTrimming/${file[$SLURM_ARRAY_TASK_ID]}_2_paired.fq.gz \
 --readFilesCommand gunzip -c \
 --outFileNamePrefix ${file[$SLURM_ARRAY_TASK_ID]} \
 --outSAMtype BAM SortedByCoordinate \
 --quantMode TranscriptomeSAM GeneCounts

# file=(*ReadsPerGene.out.tab)
# for f in ${file[@]}; do cut -f1-2 $f > $f\.txt; done

