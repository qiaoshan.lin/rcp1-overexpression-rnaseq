#!/bin/bash
#SBATCH --job-name=blastp
#SBATCH -a 0-94
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=1G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load blast
module load samtools 

#run before submit the job:
#grep '>' /home/CAM/qlin/Mimulus_Genomes/results/annotation/LF10/LF10g_v2.0beta.longest_isoform.protein.fa|tr -d '>' > LF10g_v2.0beta.longest_isoform.txt
#split -l 300 LF10g_v2.0beta.longest_isoform.txt prot

query=/home/CAM/qlin/Mimulus_Genomes/results/annotation/LF10/LF10g_v2.0beta.longest_isoform.protein.fa
subject=~/resource/Athaliana/Araport11_genes.201606.pep.fasta

file=(prot*)
readarray -t protein < ${file[$SLURM_ARRAY_TASK_ID]}
for p in "${protein[@]}"
do
	samtools faidx $query $p > tmp.$SLURM_ARRAY_TASK_ID.fa
	/isg/shared/apps/blast/ncbi-blast-2.7.1+/bin/blastp -subject $subject -query tmp.$SLURM_ARRAY_TASK_ID.fa -outfmt "6 qseqid sseqid pident" |head -1 >> LF10g_v2.0.protein.blastp_best_hit.$SLURM_ARRAY_TASK_ID.txt
done

rm tmp.$SLURM_ARRAY_TASK_ID.fa

#run after the job:
#cat LF10g_v2.0.protein.blastp_best_hit.*.txt |sort -k1,1 > LF10g_v2.0.protein.blastp_best_hit.txt
#rm LF10g_v2.0.protein.blastp_best_hit.*.txt
#awk '$3>=30{print $1,$2}' OFS="\t" LF10g_v2.0.protein.blastp_best_hit.txt > LF10g_v2.0.protein.blastp_best_hit.flt.txt
#perl -lane 'print $1,$2 if /^(.+)?\.[0-9](.+)/' LF10g_v2.0.protein.blastp_best_hit.flt.txt| join differentially_expressed_gene_list_bop_overlap_removed.txt - | awk '{print $1,$2}' OFS="\t" > differentially_expressed_gene_list_bop_overlap_removed.blastp_best_hit.txt


