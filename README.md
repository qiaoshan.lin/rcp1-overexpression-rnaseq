## Reads Quality Control

* [x] 1. Get an overview of the raw reads by [fastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) and [multiQC](https://github.com/ewels/MultiQC)

> Run [readsQC/raw/fastQC.sh](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/readsQC/raw/fastQC.sh) and [readsQC/raw/multiqc.sh](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/readsQC/raw/multiqc.sh).

Illumina universal adapters were found. 

* [x] 2. Trim reads by [Trimmomatic/0.36](http://www.usadellab.org/cms/?page=trimmomatic) *(criteria: quality score > 10 and length > 30)*

> Run [trimmed_reads/trim.sh](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/readsTrimming/trim.sh) and [readsQC/trimmed/readsQC.sh](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/readsQC/trimmed/readsQC.sh).

Most adapters were removed. 

## Differential Expression Analysis

#### Trial 1 [HISAT2](https://ccb.jhu.edu/software/hisat2/manual.shtml)+[HTSeq](https://htseq.readthedocs.io/en/release_0.9.1/count.html#usage)+[DESeq2](https://bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.html)

* [x] 1. Align reads to genome

> Run [t1/genome_index.sh](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/t1/genome_index.sh) and then [t1/hisat2.sh](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/t1/hisat2.sh).

The alignment rate is at least 98.4%.

* [x] 2. Count reads on genes

> Run: perl [t1/gtfReparse.pl](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/t1/gtfReparse.pl) [original_gtf] [reparsed_gtf] and then [t1/htseq.sh](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/t1/htseq.sh). 

> Remember to run the commands in the comments to annotate known genes. 

* [x] 3. Find differentially expressed genes

> Run [t1/DESeq2.R](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/t1/DESeq2.R) (recommend to run on a local machine). 

* [ ] 4. Find genes that might be important to the change of pollination syndromes. 

#### Trial 2 [Salmon](https://combine-lab.github.io/salmon/getting_started/)+[DESeq2](https://bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.html)

* [x] 1. Quantify expression levels (map reads to CDS by Salmon)

> Run [t2/index.sh](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/t2/index.sh) and then [t2/salmon.sh](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/t2/salmon.sh).

The alignment rates vary from 70% to 75%, which is not worse than Bowtie2. 

* [x] 2. Find differentially expressed genes

> Run [t2/DESeq2.R](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/t2/DESeq2.R) (recommend to run on a local machine). 

* [x] 3. Compare differentially expressed gene list from trial 1 and trial 2

> Run [t2/compare.R](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/t2/compare.R) (this is optional).

For the early stage, HTSeq list has 857 genes; Salmon list has 1048 genes. They have 777 genes in common. 
The genes missed by Salmon in HTSeq list have LFC up to 7. Some missed genes were caused by low count (sample counts sum<100) filtering. The threshold should be lower.  

Set the low count threshold to 50. Now Salmon list has 1109 genes. Salmon and HTSeq lists have 794 genes in common. 
The 63 missing genes still have LFC up to 7, which were caused by different raw counts in these two trials. 
I checked the reads alignments in trial 1 and found them perfect. 
Next, I check the HTSeq raw counts of the genes missed by HTSeq but existed in Salmon list. Most of these genes are also differentially expressed due to the HTSeq results. 
But not that significant. So they did not fall into the final list. 
It should be noted that the raw counts vary a lot between these two trials, which, I guess, is due to the different reference form (genome v.s. CDS). 
Thus, trial 3 is worth trying.

So far, overall, HTSeq looks more reliable than Salmon.

#### Trial 3 [STAR](https://github.com/alexdobin/STAR/blob/master/doc/STARmanual.pdf)+[DESeq2](https://bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.html)

* [x] 1. Align reads to genome

> Run [t3/index.sh](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/t3/index.sh) and then [t3/align.sh](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/t3/align.sh). Remember to run the ending comments for generating txt for DESeq2 reading.

Comparing STAR results to HISAT2 results for sample LF10_FB13A, they share 8106 genes no expression. 
211 genes only expressed in one of them. There are some genes having high count number up to 910 in HISAT2 but no count in STAR. 
For genes having counts in both of them, 9835 genes have higher counts in HISAT2 while 6818 genes have higher counts in STAR. 7283 genes have exactly the same counts.
However, genes with higher counts in STAR differ from HISAT2 more, i.e. there are 107988 STAR read counts higher than HISAT2 while only 68613 HISAT2 read counts higher than SATR. 
The count difference between HISAT2 and STAR is up to 11672 and the standard deviation is up to 40. 

* [x] 2. Find differentially expressed genes

> Run [t3/DESeq2.R](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/t3/DESeq2.R) (recommend to run on a local machine). 

* [x] 3. Compare differentially expressed gene list from trial 1 and trial 3

For the early stage, HTSeq list has 857 genes; STAR list has 855 genes. They have 835 genes in common. 
The normalized counts between these two trials are very similar. The difference of the lists is caused by the minor difference of adjusted p value around the threshold, i.e. the minor difference in counts caused the adjusted p value go <0.01 in one trial and go >0.01 in the other.

* [x] 4. Get Arabidopsis orthologs for GO enrichment analysis

> Run [t3/blastp.sh](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/t3/blastp.sh)
> Use AGRIGO to do enrichment analysis

#### Trial 4 [STAR](https://github.com/alexdobin/STAR/blob/master/doc/STARmanual.pdf)+[Salmon](https://combine-lab.github.io/salmon/getting_started/)+[DESeq2](https://bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.html)

* [x] 1. Align reads to genome

> Use STAR alignment results in trial 3. 

* [x] 2. Quantify expression levels

> Run [t4/salmon.sh](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/t4/salmon.sh).

* [x] 3. Find differentially expressed genes

> Run [t4/DESeq2.R](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/t4/DESeq2.R) (recommend to run on a local machine). 

* [x] 4. Compare differentially expressed gene lists from trial 1-4

> Run [t4/compare.R](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/t4/compare.R).

See Venn graph of gene lists in [t4/compare.png](https://gitlab.com/qiaoshan.lin/rcp1-overexpression-rnaseq/blob/master/t4/compare.png).
Overall, it's hard to say which pipeline is "right". HISAT2+HTSeq and STAR lead to similar results. Salmon leads to more differentially expressed genes but might have more false positive as well. 

I would use the gene list from either trial 1 or trial 3 to get started. If the post-process goes into trouble, I can come back to add more genes from trial 2 or trial 4. 

##### :exclamation: The statistics above is no longer accurate. Because I have changed the low count filtering criteria from `rowSums(counts) >= 100` to `rowMax(counts) > 10`.

