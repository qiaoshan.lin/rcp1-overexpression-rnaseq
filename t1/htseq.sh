#!/bin/bash
#SBATCH --job-name=htseq
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-11
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o htseq_%A_%a.out
#SBATCH -e htseq_%A_%a.err

module load htseq

file=(trimmed_LF10_FB13A trimmed_LF10_FB13B trimmed_LF10_FB13C trimmed_LF10_FB5A trimmed_LF10_FB5B trimmed_LF10_FB5C trimmed_RCP1_FB3A trimmed_RCP1_FB3B trimmed_RCP1_FB3C trimmed_RCP1_FB9A trimmed_RCP1_FB9B trimmed_RCP1_FB9C)

htseq-count \
 -f bam \
 --stranded=no \
 ../../03_align/t2/${file[$SLURM_ARRAY_TASK_ID]}.sort.bam \
 ../LF10g_v2.0.parsed.gtf > ${file[$SLURM_ARRAY_TASK_ID]}.count

# annotate known genes
#file=(*count)
#for f in ${file[@]}; do sort -k1,1b $f > $f\.sort; done
#file=(*sort)
#for f in ${file[@]}; do join -j 1 -a 2 ~/resource/LF10/LF10g_v2.0_known_gene_anno $f |sed 's/ /\t/'|cut -f2|grep '^ML'> $f\.anno; done

