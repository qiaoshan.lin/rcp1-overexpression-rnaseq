#!/opt/perl/bin/perl

my $file = $ARGV[0];
print "Usage: perl gtfReparse.pl <gtf_file> > out.gtf" if not defined $file;
open (INPUT, $file) or die "Cannot open the file $file\n";

while (<INPUT>) {
	my @row = split(/\t/, $_);
	$_ =~ s/transcript_id /transcript_id=/;
	$_ =~ s/gene_id /gene_id=/;
	print if $row[2] eq "exon";
}

close INPUT;

