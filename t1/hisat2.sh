#!/bin/bash
#SBATCH --job-name=hisat2
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-11
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=30G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o hisat2_%A_%a.out
#SBATCH -e hisat2_%A_%a.err

module load hisat2/2.0.5
module load samtools

file=(trimmed_LF10_FB13A trimmed_LF10_FB13B trimmed_LF10_FB13C trimmed_LF10_FB5A trimmed_LF10_FB5B trimmed_LF10_FB5C trimmed_RCP1_FB3A trimmed_RCP1_FB3B trimmed_RCP1_FB3C trimmed_RCP1_FB9A trimmed_RCP1_FB9B trimmed_RCP1_FB9C)

ref=/home/CAM/qlin/resource/LF10/LF10g_v2.0.fa
splicesite=/home/CAM/qlin/resource/LF10/LF10g_v2.0_splice_sites.txt

hisat2 -q --phred33 -p 8 \
 --dta \
 --known-splicesite-infile $splicesite \
 -x ./LF10g_v2.0 \
 -1 /home/CAM/qlin/RCP1_OX_Analysis/02_trim/t0/${file[$SLURM_ARRAY_TASK_ID]}_1_paired.fq.gz \
 -2 /home/CAM/qlin/RCP1_OX_Analysis/02_trim/t0/${file[$SLURM_ARRAY_TASK_ID]}_2_paired.fq.gz \
 -S ${file[$SLURM_ARRAY_TASK_ID]}.sam

samtools view -bST $ref -@ 8 ${file[$SLURM_ARRAY_TASK_ID]}.sam > ${file[$SLURM_ARRAY_TASK_ID]}.bam
rm ${file[$SLURM_ARRAY_TASK_ID]}.sam
samtools sort -n -o ${file[$SLURM_ARRAY_TASK_ID]}.sort.bam -T ${file[$SLURM_ARRAY_TASK_ID]}_tmp -O bam -@ 8 ${file[$SLURM_ARRAY_TASK_ID]}.bam
rm ${file[$SLURM_ARRAY_TASK_ID]}.bam
#samtools index ${file[$SLURM_ARRAY_TASK_ID]}.sort.bam


