#!/bin/bash
#SBATCH --job-name=index
#SBATCH -n 8
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o genome_index_%j.out
#SBATCH -e genome_index_%j.err

module load hisat2/2.0.5

ref=/home/CAM/qlin/resource/LF10/LF10g_v2.0.fa

hisat2-build -f -p 8 $ref ./LF10g_v2.0

