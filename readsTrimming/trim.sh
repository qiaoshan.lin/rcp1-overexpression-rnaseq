#!/bin/bash
#SBATCH --job-name=trimmomatic_t0
#SBATCH -n 8
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mem=40G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o t0_%j.out
#SBATCH -e t0_%j.err

module load Trimmomatic/0.36

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_FB13A.txt \
 ../raw_reads/LF10_FB13A_1.fastq.gz ../raw_reads/LF10_FB13A_2.fastq.gz \
 trimmed_LF10_FB13A_1_paired.fq.gz trimmed_LF10_FB13A_1_unpaired.fq.gz \
 trimmed_LF10_FB13A_2_paired.fq.gz trimmed_LF10_FB13A_2_unpaired.fq.gz \
 ILLUMINACLIP:./adapters.fa:2:30:10 \
 LEADING:5 \
 TRAILING:5 \
 SLIDINGWINDOW:4:10 \
 MINLEN:30

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_FB13B.txt \
 ../raw_reads/LF10_FB13B_1.fastq.gz ../raw_reads/LF10_FB13B_2.fastq.gz \
 trimmed_LF10_FB13B_1_paired.fq.gz trimmed_LF10_FB13B_1_unpaired.fq.gz \
 trimmed_LF10_FB13B_2_paired.fq.gz trimmed_LF10_FB13B_2_unpaired.fq.gz \
 ILLUMINACLIP:./adapters.fa:2:30:10 \
 LEADING:5 \
 TRAILING:5 \
 SLIDINGWINDOW:4:10 \
 MINLEN:30

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_FB13C.txt \
 ../raw_reads/LF10_FB13C_1.fastq.gz ../raw_reads/LF10_FB13C_2.fastq.gz \
 trimmed_LF10_FB13C_1_paired.fq.gz trimmed_LF10_FB13C_1_unpaired.fq.gz \
 trimmed_LF10_FB13C_2_paired.fq.gz trimmed_LF10_FB13C_2_unpaired.fq.gz \
 ILLUMINACLIP:./adapters.fa:2:30:10 \
 LEADING:5 \
 TRAILING:5 \
 SLIDINGWINDOW:4:10 \
 MINLEN:30

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_FB5A.txt \
 ../raw_reads/LF10_FB5A_1.fastq.gz ../raw_reads/LF10_FB5A_2.fastq.gz \
 trimmed_LF10_FB5A_1_paired.fq.gz trimmed_LF10_FB5A_1_unpaired.fq.gz \
 trimmed_LF10_FB5A_2_paired.fq.gz trimmed_LF10_FB5A_2_unpaired.fq.gz \
 ILLUMINACLIP:./adapters.fa:2:30:10 \
 LEADING:5 \
 TRAILING:5 \
 SLIDINGWINDOW:4:10 \
 MINLEN:30

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_FB5B.txt \
 ../raw_reads/LF10_FB5B_1.fastq.gz ../raw_reads/LF10_FB5B_2.fastq.gz \
 trimmed_LF10_FB5B_1_paired.fq.gz trimmed_LF10_FB5B_1_unpaired.fq.gz \
 trimmed_LF10_FB5B_2_paired.fq.gz trimmed_LF10_FB5B_2_unpaired.fq.gz \
 ILLUMINACLIP:./adapters.fa:2:30:10 \
 LEADING:5 \
 TRAILING:5 \
 SLIDINGWINDOW:4:10 \
 MINLEN:30

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_FB5C.txt \
 ../raw_reads/LF10_FB5C_1.fastq.gz ../raw_reads/LF10_FB5C_2.fastq.gz \
 trimmed_LF10_FB5C_1_paired.fq.gz trimmed_LF10_FB5C_1_unpaired.fq.gz \
 trimmed_LF10_FB5C_2_paired.fq.gz trimmed_LF10_FB5C_2_unpaired.fq.gz \
 ILLUMINACLIP:./adapters.fa:2:30:10 \
 LEADING:5 \
 TRAILING:5 \
 SLIDINGWINDOW:4:10 \
 MINLEN:30

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_RCP1_FB3A.txt \
 ../raw_reads/RCP1_FB3A_1.fastq.gz ../raw_reads/RCP1_FB3A_2.fastq.gz \
 trimmed_RCP1_FB3A_1_paired.fq.gz trimmed_RCP1_FB3A_1_unpaired.fq.gz \
 trimmed_RCP1_FB3A_2_paired.fq.gz trimmed_RCP1_FB3A_2_unpaired.fq.gz \
 ILLUMINACLIP:./adapters.fa:2:30:10 \
 LEADING:5 \
 TRAILING:5 \
 SLIDINGWINDOW:4:10 \
 MINLEN:30

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_RCP1_FB3B.txt \
 ../raw_reads/RCP1_FB3B_1.fastq.gz ../raw_reads/RCP1_FB3B_2.fastq.gz \
 trimmed_RCP1_FB3B_1_paired.fq.gz trimmed_RCP1_FB3B_1_unpaired.fq.gz \
 trimmed_RCP1_FB3B_2_paired.fq.gz trimmed_RCP1_FB3B_2_unpaired.fq.gz \
 ILLUMINACLIP:./adapters.fa:2:30:10 \
 LEADING:5 \
 TRAILING:5 \
 SLIDINGWINDOW:4:10 \
 MINLEN:30

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_RCP1_FB3C.txt \
 ../raw_reads/RCP1_FB3C_1.fastq.gz ../raw_reads/RCP1_FB3C_2.fastq.gz \
 trimmed_RCP1_FB3C_1_paired.fq.gz trimmed_RCP1_FB3C_1_unpaired.fq.gz \
 trimmed_RCP1_FB3C_2_paired.fq.gz trimmed_RCP1_FB3C_2_unpaired.fq.gz \
 ILLUMINACLIP:./adapters.fa:2:30:10 \
 LEADING:5 \
 TRAILING:5 \
 SLIDINGWINDOW:4:10 \
 MINLEN:30

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_RCP1_FB9A.txt \
 ../raw_reads/RCP1_FB9A_1.fastq.gz ../raw_reads/RCP1_FB9A_2.fastq.gz \
 trimmed_RCP1_FB9A_1_paired.fq.gz trimmed_RCP1_FB9A_1_unpaired.fq.gz \
 trimmed_RCP1_FB9A_2_paired.fq.gz trimmed_RCP1_FB9A_2_unpaired.fq.gz \
 ILLUMINACLIP:./adapters.fa:2:30:10 \
 LEADING:5 \
 TRAILING:5 \
 SLIDINGWINDOW:4:10 \
 MINLEN:30

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_RCP1_FB9B.txt \
 ../raw_reads/RCP1_FB9B_1.fastq.gz ../raw_reads/RCP1_FB9B_2.fastq.gz \
 trimmed_RCP1_FB9B_1_paired.fq.gz trimmed_RCP1_FB9B_1_unpaired.fq.gz \
 trimmed_RCP1_FB9B_2_paired.fq.gz trimmed_RCP1_FB9B_2_unpaired.fq.gz \
 ILLUMINACLIP:./adapters.fa:2:30:10 \
 LEADING:5 \
 TRAILING:5 \
 SLIDINGWINDOW:4:10 \
 MINLEN:30

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_RCP1_FB9C.txt \
 ../raw_reads/RCP1_FB9C_1.fastq.gz ../raw_reads/RCP1_FB9C_2.fastq.gz \
 trimmed_RCP1_FB9C_1_paired.fq.gz trimmed_RCP1_FB9C_1_unpaired.fq.gz \
 trimmed_RCP1_FB9C_2_paired.fq.gz trimmed_RCP1_FB9C_2_unpaired.fq.gz \
 ILLUMINACLIP:./adapters.fa:2:30:10 \
 LEADING:5 \
 TRAILING:5 \
 SLIDINGWINDOW:4:10 \
 MINLEN:30

