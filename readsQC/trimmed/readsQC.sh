#!/bin/bash
#SBATCH --job-name=fastqc_t0
#SBATCH -n 4
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o t0_%j.out
#SBATCH -e t0_%j.err

module load fastqc/0.11.5 
module load MultiQC/1.1

fastqc -o ./ ../../trimmomatic/t0/*_paired.fq.gz
multiqc -f -n trimmed ./trimmed*


