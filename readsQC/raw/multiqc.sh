#!/bin/bash
#SBATCH --job-name=multiqc
#SBATCH -c 2
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o multiqc_%j.out
#SBATCH -e multiqc_%j.err

module load MultiQC/1.1 

multiqc .


