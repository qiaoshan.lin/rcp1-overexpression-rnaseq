#!/bin/bash
#SBATCH --job-name=fastqc_t1
#SBATCH -n 2
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o t1_%j.out
#SBATCH -e t1_%j.err

module load fastqc/0.11.5 

fastqc -o ./ ../raw_reads/*


